#!/usr/bin/env bash
# spelling helper script
# requires aspell and aspell-en
# sudo apt install -y aspell aspell-en

FILE=${1:-no_file}

if [ "$FILE" == "no_file" ]; then
	echo "Error: No file name supplied"
	exit 1
fi

if [ ! -f "$FILE" ]; then
	echo "Error: Supplied name ($FILE) is not a file"
	exit 2
fi


aspell -l en_GB -c "${FILE}"
