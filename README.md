# sellorm.com gemini capsule content

Content goes in src/srv/gemini/content.

Content is then packaged into a deb file for deployment.

Designed for use with [agate-deb](https://codeberg.org/sellorm/agate-deb).

# Packaging

The packaging process used [nfpm](https://nfpm.goreleaser.com/) to create the deb file.

Adjust the version number in `nfpm.yml`.

